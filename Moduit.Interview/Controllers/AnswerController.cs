﻿using Microsoft.AspNetCore.Mvc;
using Moduit.Interview.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moduit.Interview.Controllers
{
    public class AnswerController : Controller
    {
        [HttpGet("QuestionOne")]
        public IActionResult QuestionOne()
        {
            string message = string.Empty;
            var result = new Answer().QuestionOne(out message);
            if (string.IsNullOrEmpty(message))
                return Ok(result);
            else
                return BadRequest(new Response { Status = 400, Message = message });
        }
        [HttpGet("QuestionTwo")]
        public IActionResult QuestionTwo()
        {
            //NOTE :
            //there is no data response with title/description "Erognomics"
            //then i filter by "ergonomic"

            string message = string.Empty;
            var result = new Answer().QuestionTwo(out message);
            if (string.IsNullOrEmpty(message))
                return Ok(result);
            else
                return BadRequest(new Response { Status = 400, Message = message });
        }
        [HttpGet("QuestionThree")]
        public IActionResult QuestionThree()
        {
            string message = string.Empty;
            var result = new Answer().QuestionThree(out message);
            if (string.IsNullOrEmpty(message))
                return Ok(result);
            else
                return BadRequest(new Response { Status = 400, Message = message });
        }
    }
}
