﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Moduit.Interview.Model
{
    public class Answer
    {
        string baseUrl;

        public Answer()
        {
            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
            var moduit = (JObject)appsettingsjson["moduit"];
            baseUrl = moduit.Property("baseUrl").Value.ToString();
        }

        public QuestionOneResponse QuestionOne(out string message)
        {
            try
            {
                message = string.Empty;
                var responseFromServer = "";

                Console.WriteLine("QuestionOne() - Start");
                Console.WriteLine("baseUrl : " + baseUrl);

                string httpVerb = "GET";

                WebRequest request = WebRequest.Create(baseUrl + "/backend/question/one");
                request.Method = httpVerb;

                request.ContentType = "application/json";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    responseFromServer = reader.ReadToEnd();
                }

                var result = JsonConvert.DeserializeObject<QuestionOneResponse>(responseFromServer);

                Console.WriteLine("QuestionOne() - End");

                return result;
            }
            catch (WebException ex)
            {
                string responseText = "";

                var responseStream = ex.Response?.GetResponseStream();

                if (responseStream != null)
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseText = reader.ReadToEnd();
                    }
                }
                message = "ERROR QuestionOne() => " + responseText;
                Console.WriteLine(message);

                message = responseText;

                return new QuestionOneResponse();
            }
        }
        public List<QuestionTwoResponse> QuestionTwo(out string message)
        {
            try
            {
                message = string.Empty;
                var responseFromServer = "";

                Console.WriteLine("QuestionTwo() - Start");
                Console.WriteLine("baseUrl : " + baseUrl);

                string httpVerb = "GET";

                WebRequest request = WebRequest.Create(baseUrl + "/backend/question/two");
                request.Method = httpVerb;

                request.ContentType = "application/json";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    responseFromServer = reader.ReadToEnd();
                }

                var result = JsonConvert.DeserializeObject<List<QuestionTwoResponse>>(responseFromServer);

                result = Filter(result);

                Console.WriteLine("QuestionTwo() - End");

                return result;
            }
            catch (WebException ex)
            {
                string responseText = "";

                var responseStream = ex.Response?.GetResponseStream();

                if (responseStream != null)
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseText = reader.ReadToEnd();
                    }
                }
                message = "ERROR QuestionTwo() => " + responseText;
                Console.WriteLine(message);

                message = responseText;

                return new List<QuestionTwoResponse>();
            }
        }
        public List<FlattenQuestionThreeResponse> QuestionThree(out string message)
        {
            try
            {
                message = string.Empty;
                var responseFromServer = "";

                Console.WriteLine("QuestionThree() - Start");
                Console.WriteLine("baseUrl : " + baseUrl);

                string httpVerb = "GET";

                WebRequest request = WebRequest.Create(baseUrl + "/backend/question/three");
                request.Method = httpVerb;

                request.ContentType = "application/json";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    responseFromServer = reader.ReadToEnd();
                }

                var result = JsonConvert.DeserializeObject<List<QuestionThreeResponse>>(responseFromServer);

                var flattenResult = Flatten(result);

                Console.WriteLine("QuestionThree() - End");

                return flattenResult;
            }
            catch (WebException ex)
            {
                string responseText = "";

                var responseStream = ex.Response?.GetResponseStream();

                if (responseStream != null)
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseText = reader.ReadToEnd();
                    }
                }
                message = "ERROR QuestionThree() => " + responseText;
                Console.WriteLine(message);

                message = responseText;

                return new List<FlattenQuestionThreeResponse>();
            }
        }
        public List<QuestionTwoResponse> Filter(List<QuestionTwoResponse> input)
        {
            try
            {

                var filter = (from a in input
                               where (a.description.ToLower().Contains("ergonomic")
                               || a.title.ToLower().Contains("ergonomic"))
                               && a.tags != null 
                               && a.tags.Contains("Sports")
                               orderby a.id
                               select a).TakeLast(3).ToList();

                return filter;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<FlattenQuestionThreeResponse> Flatten(List<QuestionThreeResponse> inputs)
        {
            try
            {
                var flatten = new List<FlattenQuestionThreeResponse>();
                foreach (var input in inputs)
                {
                    if(input.items != null)
                    {
                        foreach (var item in input.items)
                        {
                            flatten.Add(new FlattenQuestionThreeResponse
                            {
                                id = input.id,
                                category = input.category,
                                title = item.title,
                                description = item.description,
                                footer = item.footer,
                                tags = input.tags,
                                createdAt = input.createdAt
                            });
                        }
                    }
                    else
                    {
                        flatten.Add(new FlattenQuestionThreeResponse
                        {
                            id = input.id,
                            category = input.category,
                            title = string.Empty,
                            description = string.Empty,
                            footer = string.Empty,
                            tags = input.tags,
                            createdAt = input.createdAt
                        });
                    }
                }

                return flatten;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
    public class QuestionOneResponse
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string footer { get; set; }
        public List<string> tags { get; set; }
        public DateTime createdAt { get; set; }
    }
    public class QuestionTwoResponse
    {
        public int id { get; set; }
        public int category { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string footer { get; set; }
        public List<string> tags { get; set; }
        public DateTime createdAt { get; set; }
    }
    public class QuestionThreeResponse
    {
        public int id { get; set; }
        public int category { get; set; }
        public List<string> tags { get; set; }
        public List<itemsResponse> items { get; set; }
        public DateTime createdAt { get; set; }
    }
    public class itemsResponse
    {
        public string title { get; set; }
        public string description { get; set; }
        public string footer { get; set; }
    }
    public class FlattenQuestionThreeResponse
    {
        public int id { get; set; }
        public int category { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public List<string> tags { get; set; }
        public string footer { get; set; }
        public DateTime createdAt { get; set; }
    }
    public class Response
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
